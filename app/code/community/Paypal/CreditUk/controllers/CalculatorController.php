<?php

/**
 * PPC Calculator Controller
 *
 * @version     $Id$
 * @package     Paypal_CreditUk
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */

class Paypal_CreditUk_CalculatorController extends Mage_Core_Controller_Front_Action
{
	public function ajaxAction () 
	{
		try 
		{
			// Confirm we can load the required model and load a handle to it into a variable
			$calcModel = Mage::getModel('paypalcredituk/calculator');
			if(!$calcModel)
			{
				throw new Exception(__('Failed to load required model.'));
			}

			// Using the POST data, run through the following data to provide the relevant repayment information
			$value = $calcModel->getValue($this->getRequest()->getParams());
			$accessToken = $calcModel->getAccessToken();
			$repaymentInfo = $calcModel->getRepaymentInfo($accessToken, number_format($value,2,'.',''));

			// If JSON flag has been set, return a JSON object containing the repayment details
			if(!empty($this->getRequest()->getParams()['json']))
			{
				$repaymentInfo['symbol'] = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();;
				echo json_encode($repaymentInfo);
				exit();
			}
			// Output the information in table form
			echo $calcModel->getTabularData($repaymentInfo);
		}
		catch (Exception $e)
		{
			if(!$calcModel)
			{
				echo $e->getMessage();
			}
			else 
			{
				echo $calcModel->getErrorTemplate($e->getMessage());
			}
		}
	}
}