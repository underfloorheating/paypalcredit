<?php

class Paypal_CreditUk_Block_Payment_Info extends Mage_Paypal_Block_Payment_Info
{
    /**
     * @param null|Varien_Object $transport
     * @return Varien_Object
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        $transport = parent::_prepareSpecificInformation($transport);
        return $transport->addData(
            $this->getAdminBlockData()
        );
    }

    /**
     * @return array
     */
    protected function getAdminBlockData()
    {
        $creditResponseMap = array(
            //'paypal_cart_change_tolerance' => $this->__('Cart Change Tolerance'),
            //'paypal_is_financing' => $this->__('Is Financing'),
            'paypal_financing_fe_amt' => $this->__('Financing Fee'),
            'paypal_financing_term' => $this->__('Length (In Months)'),
            'paypal_financing_monthly_payment' => $this->__('Monthly Payment'),
            'paypal_financing_total_cost' => $this->__('Total Cost'),
        );

        $payment = $this->getInfo();
        $finData = array();

        foreach($creditResponseMap as $key => $label) {
            $value = $payment->getAdditionalInformation($key);
            if ($value) {
                $finData[] = $label . ': '. $payment->getAdditionalInformation($key);
            }
        }

        return array($this->__('Financing information') => $finData);
    }
}
