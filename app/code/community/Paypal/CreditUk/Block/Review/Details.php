<?php

class Paypal_CreditUk_Block_Review_Details extends Mage_Core_Block_Template
{
    /**
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote = null;

    /**
     * Return quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function getQuote()
    {
        if (is_null($this->_quote)) {
            $this->_quote = Mage::getSingleton('checkout/session')->getQuote();
        }
        return $this->_quote;
    }

    /**
     * Block initializer, just before HTML output
     *
     * @return void
     */
    protected function _beforeToHtml()
    {
        $from = array(
            $this->getQuote()->getPayment(),
            'getAdditionalInformation'
        );
        $to = array(
            $this,
            'setData'
        );
        $this->mapCreditData($from, $to);

        parent::_beforeToHtml();
    }

    /**
     * Map PayPal Credit data fields to object
     *
     * @param $fromObject callback
     * @param $toObject callback
     * @return void
     */
    protected function mapCreditData($fromObject, $toObject)
    {
        $map = array_values(Paypal_CreditUk_Model_Express_Checkout::$creditResponseMap);
        Varien_Object_Mapper::accumulateByMap($fromObject, $toObject, $map);
    }
}
