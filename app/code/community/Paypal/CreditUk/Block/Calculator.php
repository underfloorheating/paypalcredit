<?php

/**
 * PPC Calculator Block Model
 *
 * @version     $Id$
 * @package     Paypal_CreditUk
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */

class Paypal_CreditUk_Block_Calculator extends Mage_Core_Block_Template
{
	public function getDataObject()
	{
		return $this->data;
	}
	public function getLegalBlock()
	{
		return Mage::app()
        ->getLayout()
        ->createBlock('cms/block')
        ->setBlockId('ppc-legal')
        ->toHtml();
	}
}
