<?php

class Paypal_CreditUk_Model_Pro extends Mage_Paypal_Model_Pro
{
    /**
     * {@inheritdoc}
     */
    protected $_apiType = 'paypalcredituk/api_express_nvp';

    /**
     * {@inheritdoc}
     */
    protected $_configType = 'paypalcredituk/config';
}
