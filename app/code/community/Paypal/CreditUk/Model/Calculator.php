<?php

/**
 * PPC Calculator Model
 *
 * @version     $Id$
 * @package     Paypal_CreditUk
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */

class Paypal_CreditUk_Model_Calculator extends Mage_Core_Model_Abstract
{
	/**
	 * Live Credentials
	 */
	const clientId = 'AdgWUondmy1-UGSRmbq0oUGIvktc2HXOLpwcBRrP__E96R-DYkFLIV9lX8VIIiuI7GZ8xBZ1XYEpY4Gu';
	const secretKey = 'EE3Atrdexy-2ZDap8xjisKpgjtevhXy86AZ9Jx-3LohM_VF2jULutZkT4Fd0HdXOyg7B4U5k3T4HcGr1';
	const accessUrl = 'https://api.paypal.com/v1/oauth2/token';
	const paymentUrl = 'https://api.paypal.com/v1/credit/calculated-financing-options';

	/**
	 * Sandbox Credentials
	 */
	// const clientId = 'Ac4bfpOMrcJj8-4xkRiqcsdvzs0W-PzIi3dVwnykR3EjLcqbEsTzAz3ZvTfb-HaJXZoUedCuOxLo7_7h';
	// const secretKey = 'EBY2_YsLZNRshXHBZiKpiJSmm1q964Y_43nEuwoS5xliND1ckJ8cDeXMyXapqlQE77312tyqZZHF16LG';
	// const accessUrl = 'https://api.sandbox.paypal.com/v1/oauth2/token';
	// const paymentUrl = 'https://api.sandbox.paypal.com/v1/credit/calculated-financing-options';
	//
	/**
     * Get the entered value from the POST data
     *
     * Validate and extrapolate the value from the POST data and return the result
     *
     * @param  Array	$post	The POST data
     * @return float	A clean float value
     */
	public function getValue($post)
	{
		if(!isset($post['value'])) {
			throw new Exception(__('A value has not been entered.'));
		}
		$value = floatval($post['value']);
		if($value <= 0) {
			throw new Exception(__('A valid value has not been entered.'));
		}
		if($value < 99) {
			throw new Exception(__('We\'re sorry but the minimum purchase amount to be eligible for PayPal Credit is £99.'));
		}
		return $value;
	}

	/**
     * Get the PayPal API access token
     *
     * Make an API call to PayPal and then decode the JSON respone to get an access token
     *
     * @param  String	$clientID	The PPC Calculator client ID
     * @param  String	$secretKey	The PPC Calculator secret key
     * @return String	Access Token
     */
	public function getAccessToken()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, SELF::accessUrl);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"Accept: application/json",
			"Accept-Language: en_US"
			));
		curl_setopt($curl, CURLOPT_USERPWD, SELF::clientId . ':' . SELF::secretKey);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$json = curl_exec($curl);
		curl_close($curl);

		$json = json_decode($json);

		if(isset($json->access_token) && isset($json->token_type)) {
			$accessToken = $json->token_type == "Bearer" ? $json->access_token : 0;
		} else {
			throw new Exception(__('PayPal has actively refused to provide a token, please check your information.'));
		}

		if($accessToken === 0) {
			throw new Exception(__('Unable to receive a Bearer Access Token from PayPayl.'));
		}
		return $accessToken;
	}

	/**
     * Get the PayPal repayment information
     *
     * Make an API call to PayPal and then decode the JSON respone to get the repayment information relevant to the entered value.
     *
     * @param  String	$accessToken	The PPC Calculator client ID
     * @param  Float	$value	The PPC Calculator secret key
     * @return Array	Repayment information
     */
	public function getRepaymentInfo($accessToken, $value)
	{
		$data = json_encode(array(
			"financing_country_code" => "GB",
			"transaction_amount" => array(
				"value" => $value,
				"currency_code" => "GBP"
				)
			));

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, SELF::paymentUrl);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"Content-Type:application/json",
			"Authorization: Bearer ".$accessToken
			));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$json = curl_exec($curl);
		curl_close($curl);

		// throw new Exception($json);

		$json = json_decode($json);

		if(isset($json->financing_options)) {
			$foptions = reset($json->financing_options);
			$repayments = [];
			foreach($foptions->qualifying_financing_options as $option) {
				if(!isset($option->monthly_payment)) {
					continue;
				}
				$repayments[$option->credit_financing->term] = [
				floatval($option->monthly_payment->value),
				floatval($option->total_cost->value),
				floatval($option->credit_financing->apr)
				];
			}
		} else {
			throw new Exception(__('PayPal has actively refused to provide you with repayment information.'));
		}

		return $repayments;
	}

	/**
     * Get tabular data
     *
     * Using the results from the previous steps, output the information to the template.
     *
     * @param	Array	$options	Repayment options
     * @return	HTML	The repayment options table
     */
	public function getTabularData ($options)
	{
		return Mage::app()
        ->getLayout()
        ->createBlock('paypalcredituk/calculator')
        ->setData('data',$options)
        ->setTemplate('paypalcredituk/calculator/result.phtml')
        ->toHtml();
	}

	/**
     * Get error template
     *
     * Using the results from the previous steps, output the information to the template.
     *
     * @param	Array	$options	Repayment options
     * @return	HTML	The error message display
     */
	public function getErrorTemplate ($message)
	{
		return Mage::app()
        ->getLayout()
        ->createBlock('paypalcredituk/calculator')
        ->setData('data',$message)
        ->setTemplate('paypalcredituk/calculator/error.phtml')
        ->toHtml();
	}
}