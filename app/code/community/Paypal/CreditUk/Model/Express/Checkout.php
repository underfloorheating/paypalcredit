<?php

class Paypal_CreditUk_Model_Express_Checkout extends Mage_Paypal_Model_Express_Checkout
{
    /**
     * {@inheritdoc}
     */
    protected $_apiType = 'paypalcredituk/api_express_nvp';

    /**
     * {@inheritdoc}
     */
    protected $_methodType = Paypal_CreditUk_Model_Config::METHOD_CREDIT;

    /**
     * Fields stored to additional info
     *
     * @var array
     */
    public static $creditResponseMap = array(
        'cart_change_tolerance' => 'paypal_cart_change_tolerance',
        'is_financing' => 'paypal_is_financing',
        'financing_fe_amt' => 'paypal_financing_fe_amt',
        'financing_term' => 'paypal_financing_term',
        'financing_monthly_payment' => 'paypal_financing_monthly_payment',
        'financing_total_cost' => 'paypal_financing_total_cost',
        'payer_email' => 'paypal_payer_email'
    );

    /**
     * {@inheritdoc}
     */
    public function returnFromPaypal($token)
    {
        // @todo: also uses quote->save
        parent::returnFromPaypal($token);

        $this->importPayPalCreditData($this->_api, $this->_quote->getPayment());

        // @todo: uses quote->save the second time, should be able to do it just once instead
        $this->_quote
            ->setDataChanges(true)
            ->save();
    }

    /**
     * Use a mapping array and getter/setter methods to
     * populate the AdditionalInformation object
     *
     * @param Mage_Paypal_Model_Api_Nvp $from
     * @param Mage_Payment_Model_Info $payment
     */
    protected function importPayPalCreditData(
        Mage_Paypal_Model_Api_Nvp $from,
        Mage_Payment_Model_Info $payment
    ) {
        if (is_object($from)) {
            $from = array($from, 'getDataUsingMethod');
        }
        Varien_Object_Mapper::accumulateByMap(
            $from,
            array($payment, 'setAdditionalInformation'),
            self::$creditResponseMap
        );
    }

    /**
     *
     * @param string $token
     * @param bool $commit
     * @return string
     */
    public function getCreditCheckoutUrl($token, $commit = null)
    {
        $userAction = ($commit !== null) ? 'useraction=' . $commit . '&' : '';
        $sandbox = (bool) Mage::helper('payment')
            ->getMethodInstance(Paypal_CreditUk_Model_Config::METHOD_CREDIT)
            ->getConfigData('sandbox_flag');
        return 'https://www.' . (($sandbox) ? 'sandbox.' : '')
            . 'paypal.com/checkoutnow?' . $userAction . 'token=' . $token;
    }
}
