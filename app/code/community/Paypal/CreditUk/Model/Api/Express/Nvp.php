<?php

class Paypal_CreditUk_Model_Api_Express_Nvp extends Mage_Paypal_Model_Api_Nvp
{
    /**
     * {@inheritdoc}
     *
     * @todo: PAYMENTREQUEST_0_AMT Appears not to work but according to
     * @todo: https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/ it should be deprecated
     */
    protected $_paymentInformationResponse = array(
        'PAYERID', 'PAYERSTATUS', 'CORRELATIONID', 'ADDRESSID', 'ADDRESSSTATUS',
        'PAYMENTSTATUS', 'PENDINGREASON', 'PROTECTIONELIGIBILITY', 'EMAIL', 'SHIPPINGOPTIONNAME', 'TAXID', 'TAXIDTYPE',
        'CARTCHANGETOLERANCE', 'PAYMENTINFO_0_ISFINANCING', 'PAYMENTINFO_0_FINANCINGFEEAMT',
        'PAYMENTINFO_0_FINANCINGTERM', 'PAYMENTINFO_0_FINANCINGMONTHLYPAYMENT', 'PAYMENTINFO_0_FINANCINGTOTALCOST',
    );

    /**
     * {@inheritdoc}
     */
    protected $_setExpressCheckoutRequest = array(
        'PAYMENTACTION', 'AMT', 'CURRENCYCODE', 'RETURNURL', 'CANCELURL', 'INVNUM',
        'SOLUTIONTYPE', 'NOSHIPPING', 'GIROPAYCANCELURL', 'GIROPAYSUCCESSURL', 'BANKTXNPENDINGURL',
        'PAYMENTREQUEST_0_ALLOWEDPAYMENTMETHOD', 'PAGESTYLE', 'HDRIMG', 'HDRBORDERCOLOR', 'HDRBACKCOLOR',
        'PAYFLOWCOLOR', 'LOCALECODE', 'BILLINGTYPE', 'SUBJECT', 'ITEMAMT', 'SHIPPINGAMT', 'TAXAMT', 'REQBILLINGADDRESS',
        'USERSELECTEDFUNDINGSOURCE', 'LOGOIMG',
    );

    /**
     * {@inheritdoc}
     */
    protected $_refundTransactionRequest = array(
        'TRANSACTIONID', 'REFUNDTYPE', 'CURRENCYCODE', 'NOTE', 'INVOICEID'
    );

    /**
     * {@inheritdoc}
     */
    protected $_doCaptureRequest = array(
        'AUTHORIZATIONID', 'COMPLETETYPE', 'AMT', 'CURRENCYCODE', 'NOTE', 'INVNUM', 'MSGSUBID'
    );

    /**
     * Init
     */
    public function _construct()
    {
        $this->_globalMap = array_merge($this->_globalMap, array(
            // Request fields
            'PAYMENTREQUEST_0_ALLOWEDPAYMENTMETHOD' => 'allowed_payment_method',
            'CARTCHANGETOLERANCE' => 'cart_change_tolerance',
            'INVOICEID' => 'order_number',
            'USERSELECTEDFUNDINGSOURCE' => 'funding_source',
            'LOGOIMG' => 'logo_image',
            'MSGSUBID' => 'uuid',

            // Response fields
            'PAYMENTINFO_0_ISFINANCING' => 'is_financing',
            'PAYMENTINFO_0_FINANCINGFEEAMT' => 'financing_fe_amt',
            'PAYMENTINFO_0_FINANCINGTERM' => 'financing_term',
            'PAYMENTINFO_0_FINANCINGMONTHLYPAYMENT' => 'financing_monthly_payment',
            'PAYMENTINFO_0_FINANCINGTOTALCOST' => 'financing_total_cost'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        return '123.0';
    }

    /**
     * @return string
     */
    public function getAllowedPaymentMethod()
    {
        return (strtolower($this->getPaymentAction()) === 'sale') ? 'InstantPaymentOnly' : null;
    }

    /**
     * @return string
     */
    public function getFundingSource()
    {
        return 'Finance';
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->getPayment()->getOrder()->getIncrementId();
    }

    /**
     * @return string
     */
    public function getLogoImage()
    {
        $image = $this->getHdrimg();
        if ($image) {
            return $image;
        }
        
        $logo = Mage::getStoreConfig('design/header/logo_src');
        if (empty($logo)) {
            return null;
        }
        return Mage::getDesign()->getSkinUrl($logo, array('_secure' => true));
    }

    /**
     * Create v4 UUID (RFC 4211)
     * @return string
     */
    public function getUuid()
    {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}
