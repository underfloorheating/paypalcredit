<?php

class Paypal_CreditUk_Model_Method_Credit extends Mage_Paypal_Model_Express
{
    const XML_ELIGIBILITY_THRESHOLD = 'payment/paypal_credituk/transaction_eligibility_threshold';

    /**
     * @var string
     */
    protected $_code = Paypal_CreditUk_Model_Config::METHOD_CREDIT;

    /**
     * @var string
     */
    protected $_infoBlockType = 'paypalcredituk/payment_info';

    /**
     * @var string
     */
    protected $_formBlockType = 'paypalcredituk/form';

    /**
     * @var string
     */
    protected $_proType = 'paypalcredituk/pro';

    /**
     * {@inheritdoc}
     */
    public function isAvailable($quote = null)
    {
        if (((bool) Mage::getStoreConfig('payment/paypal_express_bml/active')) &&
            Mage::helper('paypal')->getConfigurationCountryCode() == 'GB' &&
            $this->isTransactionEligible($quote)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    public function isTransactionEligible($quote = null)
    {
        if ($quote === null) {
            /* @var Mage_Sales_Model_Quote $quote */
            $quote = Mage::getSingleton('checkout/session')->getQuote();
        }

        $limit = Mage::getStoreConfig(self::XML_ELIGIBILITY_THRESHOLD);
        if (is_null($limit) || $limit === "") {
            return true;
        }

        return (float)$limit <= (float)$quote->getGrandTotal();
    }

    /**
     * {@inheritdoc}
     */
    public function getCheckoutRedirectUrl()
    {
        return Mage::getUrl('paypalcredituk/express/startMark');
    }
}
